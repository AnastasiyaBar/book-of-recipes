import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './home-page.component';
import { RecipeDetailComponent } from './recipe-detail.component';

const routs: Routes = [
	{
		path: 'book-of-recipes/recipe/:id',
        component: RecipeDetailComponent
    },
    {
    	path: 'book-of-recipes',
        component: HomePageComponent
    },
    {
        path: '',
        redirectTo: '/book-of-recipes',
        pathMatch: 'full'
    }
];

@NgModule({
	imports: [ RouterModule.forRoot(routs) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}