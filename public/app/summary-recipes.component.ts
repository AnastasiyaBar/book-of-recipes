import { Component, OnInit } from '@angular/core';

import { Recipe } from './recipe';
import { RecipeService } from './recipe.service';

@Component({
	selector: 'summary-recipes',
	templateUrl: './summary-recipes.component.html',
	styleUrls: ['./app.component.css']
})
export class SummaryRecipesComponent implements OnInit{
	recipes: Recipe[];

	constructor(private recipeService: RecipeService) {}

	getRecipes(): void {
		this.recipeService.getRecipes().then(recipes => this.recipes = recipes);
	}

	ngOnInit(): void {
		this.getRecipes();
	}
}