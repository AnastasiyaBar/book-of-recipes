import { Component } from '@angular/core';

@Component({
	selector: 'my-header',
	templateUrl: 'my-header.component.html',
	styleUrls: ['./app.component.css']
})
export class MyHeaderComponent {}