import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu.component';
import { SummaryRecipesComponent } from './summary-recipes.component';
import { MyFooterComponent } from './my-footer.component';
import { MyHeaderComponent } from './my-header.component';
import { HomePageComponent } from './home-page.component';
import { RecipeDetailComponent } from './recipe-detail.component';

import { RecipeService } from './recipe.service';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    SummaryRecipesComponent,
    MyFooterComponent,
    MyHeaderComponent,
    HomePageComponent,
    RecipeDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }