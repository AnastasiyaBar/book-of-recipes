export class Recipe {
	constructor(
		public id: number,
		public name: string,
		public hours: number,
		public minutes: number,
		public category: string,
		public complexity: string,
		public ingredients: string[],
		public quantityOfIngredient: string[],
		public cooking: string[],
		public imgSrc: string
		) { }
}